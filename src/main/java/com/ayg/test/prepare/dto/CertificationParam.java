package com.ayg.test.prepare.dto;

public class CertificationParam extends BaseParam {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cerRequestId;

	/**
	 * @return the cerRequestId
	 */
	public String getCerRequestId() {
		return cerRequestId;
	}

	/**
	 * @param cerRequestId the cerRequestId to set
	 */
	public void setCerRequestId(String cerRequestId) {
		this.cerRequestId = cerRequestId;
	}
	
}
