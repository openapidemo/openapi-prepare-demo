package com.ayg.test.prepare.dto;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.ayg.test.prepare.utils.AppConstants;
import com.ayg.test.prepare.utils.AppConstants.HttpStatusType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class PrepareWrapperResponseEntity<T> {

	// 状态
	private String code = HttpStatusType.SUCCESS.getCode();

	// 返回信息
	@JsonInclude(Include.NON_NULL)
	private String msg;

	// 响应数据
	@JsonInclude(Include.NON_NULL)
	private T data;
	
	public PrepareWrapperResponseEntity(){}
	
	public PrepareWrapperResponseEntity(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}
	
	public PrepareWrapperResponseEntity(HttpStatusType type) {
		super();
		this.code = type.getCode();
		this.msg = type.getMsg();
	}
	
	public PrepareWrapperResponseEntity(String code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public PrepareWrapperResponseEntity(T data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public boolean successed(){
		return StringUtils.equals(this.code, AppConstants.HttpStatusType.SUCCESS.getCode());
	}

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
