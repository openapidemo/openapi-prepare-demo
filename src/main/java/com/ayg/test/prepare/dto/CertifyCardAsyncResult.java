package com.ayg.test.prepare.dto;

import com.ayg.test.prepare.utils.AppConstants.HttpStatusType;

public class CertifyCardAsyncResult extends BaseResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String certResult;
	private String certResultMsg;

	public CertifyCardAsyncResult() {
		
	}
	
	public CertifyCardAsyncResult(String certResult, String certResultMsg) {
		this.certResult = certResult;
		this.certResultMsg = certResultMsg;
	}
	
	public CertifyCardAsyncResult(HttpStatusType type) {
		setCertResult(type.getCode());
		setCertResultMsg(type.getMsg());
	}
	
	/**
	 * @return the certResult
	 */
	public String getCertResult() {
		return certResult;
	}

	/**
	 * @param certResult the certResult to set
	 */
	public void setCertResult(String certResult) {
		this.certResult = certResult;
	}

	/**
	 * @return the certResultMsg
	 */
	public String getCertResultMsg() {
		return certResultMsg;
	}

	/**
	 * @param certResultMsg the certResultMsg to set
	 */
	public void setCertResultMsg(String certResultMsg) {
		this.certResultMsg = certResultMsg;
	}
	
}
