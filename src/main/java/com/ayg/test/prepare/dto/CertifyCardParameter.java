package com.ayg.test.prepare.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CertifyCardParameter extends BaseParam {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4174020214518654416L;

	private String name;
	private String idcard;
	private String validType;
	private String mobile;
	private String payAccountType;
	private String payAccount;
	private String bankName;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the idcard
	 */
	public String getIdcard() {
		return idcard;
	}

	/**
	 * @param idcard the idcard to set
	 */
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	/**
	 * @return the validType
	 */
	public String getValidType() {
		return validType;
	}

	/**
	 * @param validType the validType to set
	 */
	public void setValidType(String validType) {
		this.validType = validType;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the payAccountType
	 */
	public String getPayAccountType() {
		return payAccountType;
	}

	/**
	 * @param payAccountType the payAccountType to set
	 */
	public void setPayAccountType(String payAccountType) {
		this.payAccountType = payAccountType;
	}

	/**
	 * @return the payAccount
	 */
	public String getPayAccount() {
		return payAccount;
	}

	/**
	 * @param payAccount the payAccount to set
	 */
	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	@JsonIgnore
	public String hashString() {
		return this.idcard + this.name + getExtrSystemId() + this.validType + this.payAccountType + this.payAccount;
	}
	
}
