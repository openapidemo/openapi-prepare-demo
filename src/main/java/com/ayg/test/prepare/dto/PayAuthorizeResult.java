package com.ayg.test.prepare.dto;

public class PayAuthorizeResult extends BaseResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String orderNo;
	private String authorizationNo;

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getAuthorizationNo() {
		return authorizationNo;
	}

	public void setAuthorizationNo(String authorizationNo) {
		this.authorizationNo = authorizationNo;
	}

}
