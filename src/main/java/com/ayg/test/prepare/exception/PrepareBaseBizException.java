package com.ayg.test.prepare.exception;

import com.ayg.test.prepare.utils.AppConstants.HttpStatusType;

public class PrepareBaseBizException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	
	public PrepareBaseBizException(String code, String message) {
		super(message);
		this.code = code;
	}
	
	public PrepareBaseBizException(HttpStatusType status) {
		super(status.getMsg());
		this.code = status.getCode();
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
}
