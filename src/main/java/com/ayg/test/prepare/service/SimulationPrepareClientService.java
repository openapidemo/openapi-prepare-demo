package com.ayg.test.prepare.service;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ayg.test.econtract.utils.Constants;
import com.ayg.test.prepare.dto.CertificationParam;
import com.ayg.test.prepare.dto.CertifyCardAsyncResult;
import com.ayg.test.prepare.dto.CertifyCardParameter;
import com.ayg.test.prepare.dto.PayAuthorizeParam;
import com.ayg.test.prepare.dto.PayAuthorizeResult;
import com.ayg.test.prepare.dto.PrepareWrapperResponseEntity;
import com.ayg.test.prepare.utils.AppConstants;
import com.ayg.test.utils.HttpClientUtil;
import com.ayg.test.utils.RSADigestUtil;
import com.ayg.test.utils.TokenGenerator;

public class SimulationPrepareClientService {

	public void doPaymentAuthorize(String extrSystemId, String orderNo, String name, String amount, 
			String idcard, String mobile, String payAccount) {
		try {
			PayAuthorizeParam param = new PayAuthorizeParam();
			param.setSignType("RSA");
			param.setExtrSystemId(extrSystemId);
			param.setOrderNo(orderNo);
			param.setName(name);
			param.setAmount(amount);
			param.setIdcard(idcard);
			param.setMobile(mobile);
			param.setPayAccount(payAccount);
			param.setNonce(TokenGenerator.generate("nonce"));
			param.setTime(FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss").format(new Date()));
			param.setTimestamp(String.valueOf(System.currentTimeMillis()));
			param.setRequestId(TokenGenerator.generate("") + TokenGenerator.generate(""));
			param.setSign(RSADigestUtil.digest(param, AppConstants.CLIENT_PRIVATE_KEY));
			Map<String, String> ret = HttpClientUtil.sendPostStr(AppConstants.TEST_HOST + "/prepare/payment/authorize", 
					JSON.toJSONString(param), AppConstants.ENCODING, AppConstants.DEFAULT_CONTENT_TYPE);
			doResponseBody(ret, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doAsynCertification(String extrSystemId, String name, String idcard, String mobile, 
			String payAccountType, String payAccount, String bankName, String validType) {
		try {
			CertifyCardParameter param = new CertifyCardParameter();
			param.setValidType(validType);
			param.setName(name);
			param.setIdcard(idcard);
			param.setMobile(mobile);
			param.setPayAccountType(payAccountType);
			param.setPayAccount(payAccount);
			param.setBankName(bankName);
			param.setRequestId(TokenGenerator.generate(""));
			param.setExtrSystemId(extrSystemId);
			param.setNotifyUrl(AppConstants.NOTIFY_URL);
			param.setTimestamp(String.valueOf(System.currentTimeMillis()));
			param.setNonce(TokenGenerator.generate("nonce"));
			param.setSign(RSADigestUtil.digest(param, AppConstants.CLIENT_PRIVATE_KEY));
			System.err.println(param.getRequestId());
			Map<String, String> ret = HttpClientUtil.sendPostStr(AppConstants.PRODUCT_HOST + "/prepare/asyn/certification", 
					JSON.toJSONString(param), AppConstants.ENCODING, AppConstants.DEFAULT_CONTENT_TYPE);
			doResponseBody(ret, 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doQryCertificationResult(String extrSystemId, String cerRequestId) {
		try {
			CertificationParam param = new CertificationParam();
			param.setRequestId(TokenGenerator.generate(""));
			param.setExtrSystemId(extrSystemId);
			param.setNotifyUrl(AppConstants.NOTIFY_URL);
			param.setTimestamp(String.valueOf(System.currentTimeMillis()));
			param.setNonce(TokenGenerator.generate("nonce"));
			param.setCerRequestId(cerRequestId);
			param.setSign(RSADigestUtil.digest(param, AppConstants.CLIENT_PRIVATE_KEY));
			Map<String, String> ret = HttpClientUtil.sendPostStr(AppConstants.TEST_HOST + "/prepare/certification/result", 
					JSON.toJSONString(param), AppConstants.ENCODING, AppConstants.DEFAULT_CONTENT_TYPE);
			doResponseBody(ret, 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void doResponseBody(Map<String, String> ret, int flag) throws Exception {
		String result = JSON.toJSONString(ret.get("body")).replaceAll("\\\\", "");
		System.out.println(result);
		String _result = result.substring(1, result.length() - 1);
		PrepareWrapperResponseEntity<?> response = JSON.parseObject(_result, PrepareWrapperResponseEntity.class);
		if (StringUtils.equals(response.getCode(), "0000")) {
			JSONObject data = (JSONObject)response.getData();
			if (flag == 1) {
				PayAuthorizeResult pr = JSON.toJavaObject(data, PayAuthorizeResult.class);
				RSADigestUtil.check(RSADigestUtil.toSignStr(pr), pr.getSign(), Constants.AYG_PUBLICK_KEY);
				System.err.println(pr.getAuthorizationNo());
			} else if (flag == 2) {
				CertifyCardAsyncResult cr = JSON.toJavaObject(data, CertifyCardAsyncResult.class);
				RSADigestUtil.check(RSADigestUtil.toSignStr(cr), cr.getSign(), Constants.AYG_PUBLICK_KEY);
				System.err.println(cr.getRequestId());
			}
		}
		System.err.println();
	}
	
}
