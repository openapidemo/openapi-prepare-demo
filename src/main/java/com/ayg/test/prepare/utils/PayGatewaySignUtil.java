package com.ayg.test.prepare.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ayg.test.prepare.exception.PrepareBaseBizException;
import com.ayg.test.prepare.utils.AppConstants.HttpStatusType;

/**
 * 签名校验内部工具类
 * 
 * @author cool
 *
 */
public class PayGatewaySignUtil {

	private static final Logger logger = LoggerFactory.getLogger(PayGatewaySignUtil.class.getName());

	public static final String SIGN_KEY = "sign";
	
	private PayGatewaySignUtil() {}

	public static void check(Object obj, String securityCheckKey) {

		String sign;
		try {
			sign = getDeclaredField(obj, "sign").get(obj).toString();
		} catch (Exception e1) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名或校验参数不合法");
		}

		if (sign == null || "".equals(sign)) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名或校验参数不合法");
		}

		try {
			String digest = digest(obj, securityCheckKey);
			if (!digest.equals(sign)) {
				logger.error("签名摘要计算结果：{}", digest);
				throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名校验失败");
			}
		} catch (Exception e) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名校验不通过");
		}
	}

	public static Field getDeclaredField(Object object, String fieldName) {
		Field field = null;

		Class<?> clazz = object.getClass();

		for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
			try {
				field = clazz.getDeclaredField(fieldName);
				field.setAccessible(true);
				return field;
			} catch (Exception e) {
				// 这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
				// 如果这里的异常打印或者往外抛，则就不会执行clazz =
				// clazz.getSuperclass(),最后就不会进入到父类中了

			}
		}

		return null;
	}

	public static String digest(Object obj, String securityCheckKey) {

		if (obj == null || StringUtils.isBlank(securityCheckKey)) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名或校验参数不合法");
		}

		Map<String, Object> parameters;
		try {
			parameters = transBean2Map(obj);
		} catch (Exception e) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名系统异常");
		}
		String sign = digest(parameters, securityCheckKey);
		return sign;
	}

	public static <T> String digest(Map<String, T> dataMap, String securityCheckKey) {
		if (dataMap == null) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名数据不能为空");
		}
		if (dataMap.isEmpty()) {
			return null;
		}
		if (securityCheckKey == null) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名安全校验码数据不能为空");
		}

		TreeMap<String, T> treeMap = new TreeMap<String, T>(dataMap);
		StringBuilder sb = new StringBuilder();
		for (Entry<String, T> entry : treeMap.entrySet()) {
			if (entry.getValue() == null) {
				throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), entry.getKey() + " 待签名值不能为空");
			}
			if (entry.getKey().equals(SIGN_KEY)) {
				continue;
			}
			sb.append(entry.getKey()).append("=").append(entry.getValue().toString()).append("&");
		}
		sb.deleteCharAt(sb.length() - 1);

		sb.append(securityCheckKey);

		byte[] toDigest;
		try {
			String str = sb.toString();
			toDigest = str.getBytes(StandardCharsets.UTF_8.name());
			if (logger.isDebugEnabled()) {
				logger.debug("待签名url:" + str);
			}
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(toDigest);
			return new String(Hex.encodeHex(md.digest()));
		} catch (Exception e) {
			throw new PrepareBaseBizException(HttpStatusType.SIGNATURE_ERROR.getCode(), "签名失败");
		}
	}

	// Bean --> Map 1: 利用Introspector和PropertyDescriptor 将Bean --> Map
	public static Map<String, Object> transBean2Map(Object obj) throws Exception {

		if (obj == null) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
			PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor property : propertyDescriptors) {
				String key = property.getName();

				// 过滤class属性
				if (!key.equals("class")) {
					// 得到property对应的getter方法
					Method getter = property.getReadMethod();
					Object value = getter.invoke(obj);

					if (null != value && !StringUtils.isEmpty(String.valueOf(value)) && !key.equals("sign")) {
						map.put(key, value);
					}
				}

			}
		} catch (Exception e) {
			throw e;
		}

		return map;

	}

}
