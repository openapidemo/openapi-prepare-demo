package com.ayg.test.prepare.utils;

public class AppConstants {

	public static final String PRODUCT_HOST 			= "https://hmjsjopenapi.aiyuangong.net";
    public static final String TEST_HOST 				= "https://hmjsjopenapitest.aiyuangong.net";
    public static final String NOTIFY_URL				= PRODUCT_HOST + "/prepare/test/async/request";
    public static final String ENCODING					= "UTF-8";
    public static final String DEFAULT_CONTENT_TYPE 	= "application/json";
    public static final String DEFAULT_EXTR_SYSTEM_ID	= "";
    public static final String CLIENT_PRIVATE_KEY 		= ""; // 客户自己的私钥
    public static final String AYG_PUBLICK_KEY 			= "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDgjEtwq/rfKCvpp4sgize3vpIV" 
    		+ "iiPJM/KrBmX+oQtDFe32bQDwfPArwuZyB3n1mLEArU8i8UwMDv8PeNTFV1XlUU8F"  
    		+ "yHJOLfrxfixB3Ai/V9t7ibZXwzucbLE49OcrQlcnZvstJI5i0ZwLMjJp5OBr/3yG"  
    		+ "XCiR5wu9OCmUd4v2gQIDAQAB"; // 爱员工的公钥
    
	public enum AttachmentType {
		idcard1,idcard2,contract
	}
	
	public enum PayAccountType {
		BANK_CARDNO, ALIPAY_USERID, ALIPAY_LOGONID;
	}
	
	public enum HttpStatusType {
		
		SUCCESS("0000", "授权成功"),
		ILLEGAL_ACCESS("1001", "非法接入Appid"),
		SIGNATURE_ERROR("1002", "签名错误"),
		SYSTEM_EXCEPTION("1003", "系统异常,请稍候重试"),
		ILLEGAL_PARAM("1004", "请求参数为空或非法"),
		REAL_NAME_AUTH_FAILED("2001", "实名认证失败"),
		REAL_NAME_AUTH_QRY_FAILED("2002", "实名认证结果查询失败"),
		REAL_NAME_AUTH_ING("0", "实名认证中"),
		REAL_NAME_AUTH_PASS("1", "实名认证通过"),
		REAL_NAME_AUTH_NOPASS("2", "实名认证不通过"),
		REAL_NAME_AUTH_EXCEPTION("3", "实名认证异常，请稍后重试"),
		PAYMENT_AUTHORIZATION_NO_FAILED("3001", "申请支付授权失败"),
		NOTIFY_EXTR_ERROR("4001", "通知外部系统失败"),
		SYSTEM_ERROR("5001", "服务端系统异常");
		
		private String code;
		private String msg;
		
		private HttpStatusType(String code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		public String getCode() {
			return code;
		}

		public String getMsg() {
			return msg;
		}
	}
	
	public interface Const {
		public static final String UTF8 = "utf-8";		
	}
	
}
