package com.ayg.test.utils;


import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpClientUtil {
    // 请求超时时间
    private static int connectionRequestTimeout = 90000;

    // 连接超时时间，默认10秒
    private static int socketTimeout = 90000;

    // 传输超时时间，默认30秒
    private static int connectTimeout = 90000;

    @SuppressWarnings("deprecation")
	public static Map<String, String> sendPostStr(String targetUrl, String str, String enCoding, String contentType) {
        org.apache.http.HttpResponse response = null;
        CloseableHttpClient httpClient = null;
        HttpPost httpPost = null;
        Map<String, String> ret = new HashMap<String, String>();
        try {
            httpClient = HttpClientBuilder.create().build();
            httpPost = new HttpPost(targetUrl);
            httpPost.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectionRequestTimeout)
                    .setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build());

            HttpEntity httpEntity = new StringEntity(str, enCoding);
            httpPost.addHeader("Content-Type", contentType);
            httpPost.setEntity(httpEntity);
            response = httpClient.execute(httpPost);
            // 调用request的abort方法
            String code = response.getStatusLine().getStatusCode() + "";
            String body = "";
            ret.put("code", code);
            body = EntityUtils.toString(response.getEntity());
            ret.put("body", body);
            if ("200".equals(code)) {


            } else if ("302".equals(code)) {
                ret.put("location", response.getFirstHeader("Location").getValue());
            }
        } catch (ClientProtocolException e) {
           e.printStackTrace();
        } catch (IOException e) {
           e.printStackTrace();
        } finally {
            if (null != httpPost) {
                httpPost.abort();
            }
            if (null != httpClient) {
                httpClient.getConnectionManager().shutdown();
            }
        }
        return ret;
    }

    @SuppressWarnings("deprecation")
	public static void postMultipleFiles(String remoteUrl, String extrSystemId, String name, 
			String identity, String identityType, String sign, File frontFile, File backFile) {
        org.apache.http.HttpResponse response = null;
        CloseableHttpClient httpClient = null;
        HttpPost httpPost = null;
        HttpEntity resEntity = null;
        try {

            Charset utf8 = Charset.forName("UTF-8");
            ConnectionConfig connectionConfig = ConnectionConfig.custom().setCharset(utf8).build();
            httpClient = HttpClients.custom().setDefaultConnectionConfig(connectionConfig).build();

            MultipartEntityBuilder builder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE).setCharset(utf8);
            ContentType contentType = ContentType.create("multipart/form-data", utf8);
            builder.setContentType(contentType);

            builder.addPart("frontfile",new FileBody(frontFile));
            builder.addPart("backfile",new FileBody(backFile));
            builder.addPart("extrSystemId",new StringBody(extrSystemId, utf8));
            builder.addPart("sign",new StringBody(sign, utf8));
            builder.addPart("identity",new StringBody(identity, utf8));
            builder.addPart("identityType",new StringBody(identityType, utf8));
            builder.addPart("name",new StringBody(name, utf8));

            HttpEntity reqEntity = builder.build();
            httpPost = new HttpPost(remoteUrl);
            httpPost.setEntity(reqEntity);
            httpPost.addHeader(new BasicHeader(HttpHeaders.ACCEPT_CHARSET, "utf-8"));
            httpPost.addHeader(new BasicHeader(HttpHeaders.ACCEPT_LANGUAGE, "zh-cn"));
            httpPost.addHeader(new BasicHeader(HttpHeaders.EXPECT, "100-continue"));
            httpPost.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectionRequestTimeout)
                    .setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build());

            response = httpClient.execute(httpPost);

            String code = response.getStatusLine().getStatusCode() + "";
            if ("200".equals(code)) {
                System.out.println(EntityUtils.toString(response.getEntity()));
            } else {
                // 获取响应对象
                resEntity = response.getEntity();
                if (resEntity != null) {
                    throw new RuntimeException("文件上传失败:" + EntityUtils.toString(resEntity, Charset.forName("UTF-8")));
                }
            }
        } catch (ClientProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (null != httpPost) {
                httpPost.abort();
            }
            if (null != httpClient) {
                httpClient.getConnectionManager().shutdown();
            }
            if (null != resEntity) {
                try {
                    EntityUtils.consume(resEntity);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    @SuppressWarnings("deprecation")
	public static void postMultipleFiles(String remoteUrl,String extrSystemId,String name,String identity,String identityType,String sign,File frontFile,File backFile,String notifyUrl) {
        org.apache.http.HttpResponse response = null;
        CloseableHttpClient httpClient = null;
        HttpPost httpPost = null;
        HttpEntity resEntity = null;
        try {

            Charset utf8 = Charset.forName("UTF-8");
            ConnectionConfig connectionConfig = ConnectionConfig.custom().setCharset(utf8).build();
            httpClient = HttpClients.custom().setDefaultConnectionConfig(connectionConfig).build();

            MultipartEntityBuilder builder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE).setCharset(utf8);
            ContentType contentType = ContentType.create("multipart/form-data", utf8);
            builder.setContentType(contentType);

            builder.addPart("frontfile",new FileBody(frontFile));
            builder.addPart("backfile",new FileBody(backFile));
            builder.addPart("extrSystemId",new StringBody(extrSystemId,utf8));
            builder.addPart("name",new StringBody(name,utf8));
            builder.addPart("sign",new StringBody(sign,utf8));
            builder.addPart("identity",new StringBody(identity,utf8));
            builder.addPart("identityType",new StringBody(identityType,utf8));
            builder.addPart("notifyUrl",new StringBody(notifyUrl,utf8));

            HttpEntity reqEntity = builder.build();
            httpPost = new HttpPost(remoteUrl);
            httpPost.setEntity(reqEntity);
            httpPost.addHeader(new BasicHeader(HttpHeaders.ACCEPT_CHARSET, "utf-8"));
            httpPost.addHeader(new BasicHeader(HttpHeaders.ACCEPT_LANGUAGE, "zh-cn"));
            httpPost.addHeader(new BasicHeader(HttpHeaders.EXPECT, "100-continue"));
            httpPost.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectionRequestTimeout)
                    .setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build());

            response = httpClient.execute(httpPost);

            String code = response.getStatusLine().getStatusCode() + "";
            if ("200".equals(code)) {
                System.out.println(EntityUtils.toString(response.getEntity()));
            } else {
                // 获取响应对象
                resEntity = response.getEntity();
                if (resEntity != null) {
                    throw new RuntimeException("文件上传失败:" + EntityUtils.toString(resEntity, Charset.forName("UTF-8")));
                }
            }
        } catch (ClientProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (null != httpPost) {
                httpPost.abort();
            }
            if (null != httpClient) {
                httpClient.getConnectionManager().shutdown();
            }
            if (null != resEntity) {
                // 销毁
                try {
                    EntityUtils.consume(resEntity);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static HttpResponse sendPost(String targetUrl, Map<String, String> sendMsgMaps, String enCoding) {
        CloseableHttpClient httpClient = null;
        HttpPost httpPost = null;
        org.apache.http.HttpResponse response = null;
        try {
            HttpClientBuilder.create().build();

            if (targetUrl.startsWith("https")) {
                httpClient = enableSSL();
            }
            httpPost = new HttpPost(targetUrl);
            httpPost.setConfig(RequestConfig.custom().setConnectionRequestTimeout(connectionRequestTimeout)
                    .setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build());
            List<NameValuePair> vals = new ArrayList<NameValuePair>();
            Set<String> names = sendMsgMaps.keySet();
            NameValuePair nvp = null;
            for (String name : names) {
                nvp = new BasicNameValuePair(name, sendMsgMaps.get(name));
                vals.add(nvp);
            }
            UrlEncodedFormEntity uefEntity;
            uefEntity = new UrlEncodedFormEntity(vals, enCoding);
            httpPost.setEntity(uefEntity);
            response = httpClient.execute(httpPost);
        } catch (ClientProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (null != httpPost) {
                httpPost.abort();
            }
            try {
                if (null != httpClient) {
                    httpClient.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return response;
    }

    private static HostnameVerifier hostnameVerifier = new HostnameVerifier() {
        @Override
        public boolean verify(String arg0, SSLSession arg1) {
            return true;
        }
    };

    private static TrustManager truseAllManager = new X509TrustManager() {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

    };
    
    private static CloseableHttpClient enableSSL() {
        CloseableHttpClient httpClient = null;
        try {
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[] { truseAllManager }, null);

            SSLConnectionSocketFactory sslcsf = new SSLConnectionSocketFactory(sslcontext, null, null,
                    hostnameVerifier);
            RegistryBuilder<ConnectionSocketFactory> registryBuilder = RegistryBuilder
                    .<ConnectionSocketFactory>create();
            Registry<ConnectionSocketFactory> registry = registryBuilder.register("https", sslcsf).build();
            PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(registry);
            httpClient = HttpClientBuilder.create().setConnectionManager(connManager).build();
            return httpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
