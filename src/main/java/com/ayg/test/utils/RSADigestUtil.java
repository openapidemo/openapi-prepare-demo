package com.ayg.test.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ayg.test.econtract.IObject;
import com.ayg.test.econtract.exception.BusinessException;
import com.ayg.test.econtract.utils.ConstantsErrEnum;

import sun.misc.BASE64Decoder;

@SuppressWarnings("restriction")
public final class RSADigestUtil {

	private static final Logger logger = LoggerFactory.getLogger(RSADigestUtil.class.getName());

    /**
     * 签名编码
     */
    public static final String UTF8 = "utf-8";
    /**
     * 签名key
     */
    public static final String SIGN_KEY = "sign";
    public static final String SIGN_TYPE = "signType";

    private static final String ENCODING = "UTF-8";

    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";


    public static String digest(Object obj, String privateKey) {
        try {
            String signData = sign256(toSignStr(obj), privateKey);
            return signData;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void check(String signString, String sign, String publicKey) {

        if (StringUtils.isBlank(signString)) {
            throw new BusinessException(ConstantsErrEnum.ERR_SIGNCHECK_PARAMNOTVALID.getCode(),
                    ConstantsErrEnum.ERR_SIGNCHECK_PARAMNOTVALID.getMsg() + ":" + "签名对象不能为空");
        }

        if (sign == null || "".equals(sign)) {
            throw new BusinessException(ConstantsErrEnum.ERR_SIGNCHECK_PARAMNOTVALID.getCode(),
                    ConstantsErrEnum.ERR_SIGNCHECK_PARAMNOTVALID.getMsg() + ":" + "sign对象不能为空");
        }

        if (StringUtils.isBlank(publicKey)) {
            throw new BusinessException(ConstantsErrEnum.ERR_EXTRSYSNOPUBKEY.getCode(),
                    ConstantsErrEnum.ERR_EXTRSYSNOPUBKEY.getMsg());
        }

        try {
            boolean verify = verify256(toSignStr(signString), sign, publicKey);
            if (!verify) {
                throw new RuntimeException("签名校验失败");
            }
        } catch (Exception e) {
            throw new BusinessException(ConstantsErrEnum.ERR_CHECK_NOTVALID.getCode(),
                    ConstantsErrEnum.ERR_CHECK_NOTVALID.getMsg(), e);
        }
    }

    public static String toSignStr(Object bean) throws Exception {
        StringBuilder sb = new StringBuilder();
        buildSignStr(bean, sb, 1);
        String result = sb.toString();
        logger.info("签名字符串:[{}]", result);
        return result;
    }

    @SuppressWarnings("rawtypes")
	private static void buildSignStr(Object bean, StringBuilder sb, int level) {
        if (bean == null) {
            return;
        }
        if (bean instanceof IObject) {
            Map<String, Object> map = transBean2Map(bean);
            Set<String> keySet = map.keySet();
            Iterator<String> iter = keySet.iterator();
            if (level != 1) {
                sb.append("{");
            }
            boolean isFirst = true;
            while (iter.hasNext()) {
                if (!isFirst) {
                    sb.append("&");
                }
                String key = iter.next();
                Object value = map.get(key);
                sb.append(key).append("=");
                buildSignStr(value, sb, level + 1);
                isFirst = false;
            }
            if (level != 1) {
                sb.append("}");
            }
        } else if (bean instanceof Iterable) {
            sb.append("[");
            Iterator it = ((Iterable) bean).iterator();
            boolean isFirst = true;
            while (it.hasNext()) {
                if (!isFirst) {
                    sb.append(",");
                }
                buildSignStr(it.next(), sb, level + 1);
                isFirst = false;
            }
            sb.append("]");
        } else {
            sb.append(bean);
        }

    }

    // Bean --> Map 1: 利用Introspector和PropertyDescriptor 将Bean --> Map
    private static Map<String, Object> transBean2Map(Object obj) {

        if (obj == null) {
            return null;
        }
        Map<String, Object> map = new TreeMap<>();
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();

                // 过滤class属性
                if (!key.equals("class")) {
                    // 得到property对应的getter方法
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(obj);

                    if (null != value && StringUtils.isNotBlank(String.valueOf(value)) 
                    		&& !key.equalsIgnoreCase(SIGN_KEY) && !key.equalsIgnoreCase(SIGN_TYPE)) {
                        map.put(key, value);
                    }
                }

            }
        } catch (Exception e) {
            logger.error("bean2Map excption!",e);
            throw new BusinessException("bean2Map excption!");
        }
        return map;
    }

    private static String sign256(String data, String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException,
            SignatureException, IOException {
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(getPrivateKey(privateKey));
        signature.update(data.getBytes(ENCODING));
        String ret = Base64.encodeBase64String(signature.sign());
        return ret;
    }

    private static boolean verify256(String data, String sign, String publicKey) {
        if (StringUtils.isBlank(data) || StringUtils.isBlank(sign) || StringUtils.isBlank(publicKey)) {
            return false;
        }

        try {
            Signature signetcheck = Signature.getInstance(SIGNATURE_ALGORITHM);
            signetcheck.initVerify(getPublicKey(publicKey));
            signetcheck.update(data.getBytes(ENCODING));
            return signetcheck.verify(Base64.decodeBase64(sign.getBytes(ENCODING)));
        } catch (Exception e) {
            return false;
        }
    }

	private static PublicKey getPublicKey(String key) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

	private static PrivateKey getPrivateKey(String key) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] keyBytes = (new BASE64Decoder()).decodeBuffer(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }
	
}
