1、爱员工测试[host]：https://hmjsjopenapitest.aiyuangong.net/econtract
	ps：具体接口,请参照接口文档

2、签约流程：分提交签名与结束时签名
	提交签名：是用贵方的私钥加密，【服务方】用对应的公钥验签。具体请参照文档。
	结束签名：是用【服务方】的私钥加密，贵方用【服务方】提供的公钥验签。具体参照文档。
	ps:请贵司技术人员，按照文档要求生成RSA公私钥，并将公钥提供给【服务方】。

3、extrSystemId(appid)：

4、接收异步通知报文的方法(主要是贵方notifyUrl接收)：
	public void test(HttpServletRequest request, HttpServletResponse response) {

		try {
			InputStream is = request.getInputStream();
			byte[] buf = new byte[1024];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			for (int i; (i = is.read(buf)) != -1;) {
				baos.write(buf, 0, i);
			}
			String data = baos.toString("UTF-8");
			System.err.println("y-" + data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	PS：如果是java，建议接受ContractOrderExtrResult这个对象。如果是springmvc，直接用@RequestBody即可。
	@RequestMapping(value = "/test", method = RequestMethod.POST)
	public @ResponseBody String hello(@RequestBody ContractOrderExtrResult param, HttpServletResponse response) {
		logger.info("##################模拟接受通知############ ,param is:{}", JSON.toJSONString(param));
		return "SUCCESS";
	}

	@RequestMapping(value = "/test2", method = RequestMethod.POST)
	public @ResponseBody String hello2(@RequestBody ContractUserCertExtrResult param, HttpServletResponse response) {
		logger.info("##################模拟接受通知############ ,param is:{}", JSON.toJSONString(param));
		return "SUCCESS";
	}

5、如果测试数据的问题，如身份证信息、姓名、手机号等，在联调测试时可以伪造数据。
	如果要接收短信，手机号必须是真实。
	身份证与姓名的数据，可在此网站查询：http://id.chacha138.com/