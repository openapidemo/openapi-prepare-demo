package com.ayg.test.prepare;

import org.junit.Test;

import com.ayg.test.prepare.service.SimulationPrepareClientService;
import com.ayg.test.prepare.utils.AppConstants;
import com.ayg.test.prepare.utils.AppConstants.PayAccountType;
import com.ayg.test.utils.TokenGenerator;

public class PrepareTest {

	private SimulationPrepareClientService service = new SimulationPrepareClientService();
	
	@Test
	public void testPaymentAuthorize() {
		service.doPaymentAuthorize(AppConstants.DEFAULT_EXTR_SYSTEM_ID, 
				TokenGenerator.generateSerialNumber(0), "大花猫", "1000", 
				"420821198909256XXX", "15920678400", "test@alipay.com");
	}
	
	@Test
	public void testAsynCertification() {
		// 支付宝
		service.doAsynCertification(AppConstants.DEFAULT_EXTR_SYSTEM_ID, 
				"杨**", "45040419860504XXXX", "151**************083", PayAccountType.ALIPAY_USERID.toString(), 
				"skyking246@hotmail.com", "", "");
		
		// 银行卡
		// 三要素
		service.doAsynCertification(AppConstants.DEFAULT_EXTR_SYSTEM_ID, 
				"潘**", "3623222002013XXXXX", "151**************083", PayAccountType.BANK_CARDNO.toString(), 
				"6235*********************54", "中国农业银行", "3");

		// 四要素
		service.doAsynCertification(AppConstants.DEFAULT_EXTR_SYSTEM_ID, 
				"李**", "340421197805XXXXXXX", "151**************083", PayAccountType.BANK_CARDNO.toString(), 
				"6222*********************02", "中国农业银行", "4");
	}
	
	@Test
	public void testQryCertificationResult() {
		service.doQryCertificationResult(AppConstants.DEFAULT_EXTR_SYSTEM_ID, "");
	}
	
}
